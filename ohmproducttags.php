<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_.'ohmproducttags/classes/OhmProductTagClass.php';
require_once _PS_MODULE_DIR_.'ohmproducttags/classes/ProductTagClass.php';

class OhmProductTags extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'ohmproducttags';
        $this->tab = 'quick_bulk_update';
        $this->version = '1.0.0';
        $this->author = 'OHM';
        $this->need_instance = 1;
        $this->module_key = 'd70579a19fa88df87acbce1eab272ad3';
        
        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Product Tag Display');
        $this->description = $this->l('Display a tag in the product image.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall this module');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);

        $this->lang_selected = Configuration::get('PS_LANG_DEFAULT');

        $this->secure_key = Tools::encrypt($this->name);

        $this->path = $this->local_path;
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('PRODUCTTAG_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayAfterProductThumbs');
    }

    public function uninstall()
    {
        Configuration::deleteByName('PRODUCTTAG_LIVE_MODE');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        $mess = '';
        if (((bool)Tools::isSubmit('submitProductTagModule')) == true) {
            $res = $this->postProcess();

            if (isset($res['perf_update'])) {
                $mess .= $this->displayConfirmation($this->l('Tag updated successfully.'));
            } else {
                $mess .= $this->displayConfirmation($this->l('Tag added successfully.'));
            }
        } elseif (Tools::getValue('page') == 'delete') {
            $res = OhmProductTagClass::deleteTag([(int)Tools::getValue('id_tag')]);
            if ($res) {
                $this->context->cookie->__set(
                    'redirect_success_message',
                    $this->displayConfirmation($this->l('Tag deleted successfully.'))
                );
            } else {
                $this->context->cookie->__set(
                    'redirect_success_message',
                    $this->displayError($this->l('Error deleting the tag. Please try again!'))
                );
            }

            Tools::redirect($_SERVER['HTTP_REFERER']);
        }


        $tags = OhmProductTagClass::getTagData();
        $mod_link = $this->context->link->getAdminLink('AdminModules') . '&configure=ohmproducttags';
        
        // cookie messages
        if (isset($this->context->cookie->redirect_success_message)) {
            $this->context->smarty->assign(array(
                'redirect_success_message' => $this->context->cookie->redirect_success_message,
            ));
            $this->context->cookie->__unset('redirect_success_message');
        }

        $tagdata = OhmProductTagClass::getTagData();

        $this->context->smarty->assign('tags', $tags);
        $this->context->smarty->assign('module_link', $mod_link);
        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('tagdata', $tagdata);
        $this->context->smarty->assign('renderform', $this->renderForm());

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $mess . $output;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitProductTagModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {

        $final_products = [];
        $products = Product::getSimpleProducts($this->lang_selected);
        if ($products) {
            foreach ($products as $product) {
                $final_products[] = array(
                    'id_option' => $product['id_product'],
                    'name' => $product['name']
                );
            }
        }

        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Tag settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'hidden',
                        'name' => 'id_tag',
                    ),
                    array(
                        'col' => 3,
                        'type' => 'hidden',
                        'name' => 'update_input',
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enabled'),
                        'name' => 'enabled',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'lang' => true,
                        'class' => 'tagname_class',
                        'type' => 'text',
                        'name' => 'name',
                        'label' => $this->l('Tag name'),
                        'list' => 'browsers',
                        'required' => true,
                    ),
                    array(
                        'type' => 'textarea',
                        'lang' => true,
                        'name' => 'description',
                        'label' => $this->l('Description'),
                        'cols' => 30,
                        'rows' => 50,
                    ),
                    array(
                        'type' => 'select',
                        'prefix' => '<i class="icon icon-gift"></i>',
                        'multiple' => true,
                        'class' => 'selectpicker select2-picker',
                        'name' => 'id_product[]',
                        'label' => $this->l('Products association'),
                        'options' => array(
                            'query' => $final_products,
                            'id' => 'id_option',
                            'name' => 'name',
                        ),
                        'required' => true,
                        'hint' => array($this->l('Tag will be displayed to the product(s) selected.'))
                    ),
                    array(
                        'type' => 'color',
                        'col' => 9,
                        'class' => 'gift-settings-input',
                        'name' => 'bg_color',
                        'label' => $this->l('Tag Background color'),
                    ),
                    array(
                        'type' => 'color',
                        'col' => 9,
                        'class' => 'gift-settings-input',
                        'name' => 'text_color',
                        'label' => $this->l('Tag text color'),
                    ),
                ),
                'buttons' => array(
                    array(
                        'href' => AdminController::$currentIndex.'&configure='.
                        $this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                        'title' => $this->trans('Cancel', array(), 'Admin.Actions'),
                        'icon' => 'process-icon-close'
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        if (Tools::getIsset('id_tag') && Tools::getValue('page')) {
            $data = [];
            $tag_data = new OhmProductTagClass((int)Tools::getValue('id_tag'));
            $product_tag_data = new ProductTagClass((int)Tools::getValue('id_tag'));

            if ($product_tag_data->id_product) {
                $idproduct = '';
                $xpl = explode(',', $product_tag_data->id_product);
                foreach ($xpl as $key => $_idprod) {
                    $idproduct .= (count($xpl) == ($key+1))? '"' . $_idprod . '"' : '"' . $_idprod . '"' . ',';
                }
                $idproduct = '[' . $idproduct . ']';
            }

            $data['update_input']= 'true';
            $data['id_tag']      = $tag_data->id_tag;
            $data['enabled']     = $tag_data->status;
            $data['name']        = $tag_data->name;
            $data['description'] = $tag_data->description;
            $data['id_product[]'] = Tools::jsonDecode($idproduct);
            $data['bg_color']      = $product_tag_data->bg_color;
            $data['text_color']      = $product_tag_data->text_color;

            return $data;
        }
    }

    
    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $_id_product = '';
        if (Tools::getValue('id_product')) {
            $temp_id = array_map("strip_tags", Tools::getValue('id_product'));
            foreach ($temp_id as $key => $id_product) {
                $_id_product .= (count($temp_id) == ($key+1))? $id_product : $id_product . ',';
            }
        }
     
        $name = [];
        $description = [];
        $languages = Language::getLanguages(false);

        foreach ($languages as $lang) {
            $name[$lang['id_lang']] = Tools::getValue('name_'.$lang['id_lang']);
            $description[$lang['id_lang']] = Tools::getValue('description_'.$lang['id_lang']);
        }

        // update if true, insert if false
        if (Tools::getValue('update_input')) {
            $err_ctr  = [];
            $temp_upd = [
                'status'      => (int)Tools::getValue('enabled'),
            ];
            $err_ctr[] = Db::getInstance()->update('ohm_tag', $temp_upd, 'id_tag = '.(int)Tools::getValue('id_tag'));

            $proddata = [
                'id_product' => $_id_product,
                'bg_color' => Validate::isColor(Tools::getValue('bg_color'))? Tools::getValue('bg_color') : null,
                'text_color' => Validate::isColor(Tools::getValue('text_color'))? Tools::getValue('text_color') : null
            ];
            $err_ctr[] = Db::getInstance()->update(
                'productTag',
                $proddata,
                'id_tag = '.(int)Tools::getValue('id_tag')
            );

            foreach ($languages as $lang) {
                $lang_data = [
                    'name'        => pSQL($name[$lang['id_lang']]),
                    'description' => pSQL($description[$lang['id_lang']])
                ];
                $err_ctr[] = Db::getInstance()->update(
                    'ohm_tag_lang',
                    $lang_data,
                    'id_tag = ' . (int)Tools::getValue('id_tag') . ' AND id_lang = ' . (int)$lang['id_lang']
                );
            }
            return array('perf_update' => $err_ctr);
        } else {
            $tagdata = new OhmProductTagClass();
            $tagdata->status = (int)Tools::getValue('enabled');
            $tagdata->name = array_map("strip_tags", $name);
            $tagdata->description = array_map("strip_tags", $description);
            $tagdata->save();

            $_producttag = new ProductTagClass();
            $_producttag->id_tag = (int)$tagdata->id;
            $_producttag->id_product = $_id_product;
            $_producttag->bg_color = Validate::isColor(Tools::getValue('bg_color'))? Tools::getValue('bg_color') : null;
            $_producttag->text_color = Validate::isColor(
                Tools::getValue('text_color')
            )?Tools::getValue('text_color'):null;

            return $_producttag->save();
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        // if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJquery();
            $this->context->controller->addJS($this->_path.'views/js/select2.min.js', 'all');
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/select2.min.css', 'all');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
            $this->context->controller->addJqueryPlugin('autocomplete');

        // }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');

        $this->smarty->assign('tag_id', Tools::getValue('tag_id'));
        $this->smarty->assign('securekey', $this->secure_key);
        $this->smarty->assign('ajax_url_front', $this->context->link->getModuleLink('ohmproducttags', 'tag'));
        return $this->display(__FILE__, '/views/templates/hook/script.tpl');
    }

    private function thousandsCurrencyFormat($num)
    {
        if ($num >= 1000) {
            $x = round($num);
            $x_number_format = number_format($x);
            $x_array = explode(',', $x_number_format);
            $x_parts = array('K', 'M', 'B', 'T');
            $x_count_parts = count($x_array) - 1;
            $x_display = $x;
            $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
            $x_display .= $x_parts[$x_count_parts - 1];

            return $x_display;
        }
        return $num;
    }

    public function hookDisplayAfterProductThumbs($params)
    {
        $product_list = array();
        $id_product = Tools::getValue('id_product');
        $tags = OhmProductTagClass::getTagByProductId($id_product);

        if ($tags) {
            foreach ($tags as $i => $tag) {
                $products = explode(',', $tag['id_product']);
                if ($products) {
                    foreach ($products as $id_product) {
                        $tags[$i]['products'][] = new Product($id_product, false, $this->context->language->id);
                    }
                    $tags[$i]['total_products_count'] = $this->thousandsCurrencyFormat(count($tags[$i]['products']));
                }
            }
        }

        $this->context->smarty->assign('tags', $tags);
        $this->context->smarty->assign('product_list', $product_list);
        $this->context->smarty->assign('link', Context::getContext()->link->getModuleLink('ohmproducttags', 'tag'));
        return $this->context->smarty->fetch($this->local_path.'views/templates/hook/tag.tpl');
    }
}
