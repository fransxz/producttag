<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once _PS_MODULE_DIR_ . 'ohmproducttags/ohmproducttags.php';

class OhmProductTagClass extends ObjectModel
{
    public $id_tag;
    public $status;
    public $name;
    public $description;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'ohm_tag',
        'primary' => 'id_tag',
        'multilang' => true,
        'fields' => array(
            'id_tag'        => array('type' => self::TYPE_INT),
            'status'                => array('type' => self::TYPE_INT),

            // Lang fields
            'name'           => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName'),
            'description'    => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
        )
    );



    /**
     * Return all tag list
     * @return array
     */
    public static function getTagData()
    {
        $default_lang =  Configuration::get('PS_LANG_DEFAULT');

        $sql = 'SELECT a.*, b.*, c.* FROM `' . _DB_PREFIX_ . 'ohm_tag` a
        JOIN `' . _DB_PREFIX_ . 'ohm_tag_lang` b ON a.`id_tag` = b.`id_tag`
        JOIN `' . _DB_PREFIX_ . 'productTag` c ON b.`id_tag` = c.`id_tag`
        WHERE b.`id_lang` = ' . (int)$default_lang;
        
        if ($result = Db::getInstance()->executeS($sql)) {
            return $result;
        }

        return false;
    }


    /**
     * Return all tags by id product
     * @return array
     */
    public static function getTagByProductId($id_product)
    {
        $default_lang =  Configuration::get('PS_LANG_DEFAULT');

        $sql = 'SELECT a.*, b.*, c.* FROM `' . _DB_PREFIX_ . 'ohm_tag` a
        JOIN `' . _DB_PREFIX_ . 'ohm_tag_lang` b ON a.`id_tag` = b.`id_tag`
        JOIN `' . _DB_PREFIX_ . 'productTag` c ON b.`id_tag` = c.`id_tag`
        WHERE FIND_IN_SET('. $id_product . ', c.`id_product`) AND b.`id_lang` = ' . (int)$default_lang;
        
        if ($result = Db::getInstance()->executeS($sql)) {
            return $result;
        }

        return false;
    }



    /**
     * Return all tags by id product
     * @return array
     */
    public static function getTagById($id_tag)
    {
        $default_lang =  Configuration::get('PS_LANG_DEFAULT');

        $sql = 'SELECT a.*, b.*, c.* FROM `' . _DB_PREFIX_ . 'ohm_tag` a
        JOIN `' . _DB_PREFIX_ . 'ohm_tag_lang` b ON a.`id_tag` = b.`id_tag`
        JOIN `' . _DB_PREFIX_ . 'productTag` c ON b.`id_tag` = c.`id_tag`
        WHERE a.`id_tag` = '. $id_tag . ' AND b.`id_lang` = ' . (int)$default_lang;
        
        if ($result = Db::getInstance()->executeS($sql)) {
            return $result[0];
        }

        return false;
    }


    /**
     * Delete tag rules
     * @return array
     */
    public static function deleteTag($ids)
    {
        $store_res = [];
        if (count($ids)) {
            foreach ($ids as $id) {
                $store_res[] = Db::getInstance()->delete('ohm_tag', 'id_tag = ' . (int)$id);
                $store_res[] = Db::getInstance()->delete('ohm_tag_lang', 'id_tag = ' . (int)$id);
                $store_res[] = Db::getInstance()->delete('productTag', 'id_tag = ' . (int)$id);
            }
            return true;
        } else {
            return false;
        }
    }
}
