{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="ohmproducttags-mainwrapper">
     <script type="text/javascript">
        var page = {if Tools::getIsset('page')}"{Tools::getValue('page')}"{else}''{/if};
    </script>

    {if isset($redirect_success_message) && $redirect_success_message}
        <div>{$redirect_success_message|cleanHtml nofilter}</div>
    {/if}

    {if isset($redirect_errors) && $redirect_errors}
        <div>{$redirect_errors|cleanHtml nofilter}</div>
    {/if}

    <div class="panel">
        <div class="panel-heading">
            <i class="icon icon-credit-card"></i> {l s='Product Tag Display' mod='ohmproducttags'}
            <span class="panel-heading-action">
                <a id="addTag"
                    class="list-toolbar-btn"
                    href="javascript:void(0)"
                    title="{l s='Add new tag' mod='ohmproducttags'}"
                >
                    <i class="process-icon-new "></i>
                </a>
                <a class="list-toolbar-btn"
                    title="{l s='Reload' mod='ohmproducttags'}"
                    href="javascript:location.reload();"
                >
                    <i class="process-icon-refresh"></i>
                </a>
            </span>
        </div>	
    

    	<table class="table cart_rule">
            <thead>
                <tr class="nodrag nodrop">
                    <th class="fixed-width-xs center">
                        <span class="title_box">ID</span>
                    </th>
                    <th class="fix-width-20">
                        <span class="title_box">{l s='Tag name' mod='ohmproducttags'}</span>
                    </th>
                    <th class="fixed-width-xs center">
                        <span class="title_box ">{l s='Status' mod='ohmproducttags'}</span>
                    </th>
                    <th class="fix-width-20">
                        <span class="title_box ">{l s='Description' mod='ohmproducttags'}</span>
                    </th>
                    <th></th>
                </tr>

                <tr class="nodrag nodrop filter row_hover">
                    <form action="" method="post">
                        <th class="center">
                        	<input type="text" class="filter" name="id" {if isset($tagid)}value="{$tagid|escape:'htmlall':'UTF-8'}"{/if} placeholder="Search by tag id">
                        </th>
                        <th>
                            <input type="text" class="filter" name="tagname" {if isset($tagname)}value="{$tagname|escape:'htmlall':'UTF-8'}"{/if} placeholder="Search by tag name">
                        </th>
                        <th class="center">
                            <select class="filter fixed-width-sm" name="tag_status">
                                <option value="">-</option>
                                <option value="1" {if isset($_status) && $_status == 1}selected{/if}>{l s='Enable' mod='ohmproducttags'}</option>
                                <option value="0" {if isset($_status) && $_status == 0 && $_status !== ''}selected{/if}>{l s='Disable' mod='ohmproducttags'}</option>
                            </select>
                        </th>
                        <th>--</th>
                        

                        <th class="actions">
                            <span class="pull-right">
                                {if isset($show_clear_filter)}
                                    <a href="{$module_link|escape:'htmlall':'UTF-8'}" class="btn btn-default"><i class="icon-remove"></i> Clear filter</a>
                                {/if}

                                <button type="submit" name="searchFilterButton" id="searchFilterButton" class="btn btn-default">
                                    <i class="icon-search"></i> {l s='Search' mod='ohmproducttags'}
                                </button>
                            </span>
                        </th>
                    </form>
                </tr>
            </thead>
            <tbody>

    	        	{if !empty($tags)}
                        {foreach $tags as $i => $tag}
                            <tr>
                                <td>
                                    {$tag['id_tag']|escape:'htmlall':'UTF-8'}
                                </td>
                                <td>
                                    {$tag['name']|escape:'htmlall':'UTF-8'}
                                </td>
                                <td class="pointer center">
                                    {if $tag['status']}
                                        <a class="list-action-enable  action-enabled single-status" data-id="{$tag['id_tag']|escape:'htmlall':'UTF-8'}" data-val="Disable" title="{l s='Disable' mod='ohmproducttags'}">
                                            <i class="icon-check"></i>
                                        </a>
                                    {else}
                                        <a class="list-action-enable action-disabled single-status" data-id="{$tag['id_tag']|escape:'htmlall':'UTF-8'}" data-val="Enable" title="{l s='Enable' mod='ohmproducttags'}">
                                            <i class="icon icon-remove"></i>
                                        </a>
                                    {/if}
                                </td>
                                <td>
                                    {$tag['description']|escape:'htmlall':'UTF-8'}
                                </td>
                                <td class="text-right">
                                    <div class="btn-group-action">
                                        <div class="btn-group pull-right">
                                            <a href="{$module_link|escape:'htmlall':'UTF-8'}&page=edit&id_tag={$tag['id_tag']|escape:'htmlall':'UTF-8'}" title="Edit" class="edit btn btn-default">
                                                <i class="icon-pencil"></i> Edit
                                            </a>
                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-caret-down"></i>&nbsp;
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{$module_link|escape:'htmlall':'UTF-8'}&page=delete&id_tag={$tag['id_tag']|escape:'htmlall':'UTF-8'}" onclick="if (confirm('Are you sure you want to delete `{$tag['name']|escape:'htmlall':'UTF-8'}` tag?')){ return true; }else{ event.stopPropagation(); event.preventDefault();};" title="Delete" class="delete">
                                                        <i class="icon-trash"></i> Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        {/foreach}
                    {else}
        	        	<tr>
        	        		<td colspan="7" class="center">
        	        			<div class="list-empty-msg">
        							<i class="icon-warning-sign list-empty-icon"></i>
        							{l s='No records found' mod='ohmproducttags'}
        						</div>
        	        		</td>
        	        	</tr>
                    {/if}

            </tbody>
        </table>
    </div>

    <div id="ohmproducttag_renderform">
        {$renderform|escape:'qoutes':'UTF-8' nofilter}
    </div>
</div>
