{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file='page.tpl'}

{block name='page_content_container'}

	{assign var="filter_args" value=","|explode:Tools::getValue('q')}

	<section class="featured-products clearfix">
		<div class="col-md-2 ot-cwidth20">
			<div id="productTag-wrapper">
				<div class="filter-tag-wrapper ohmtags-single-wrapper">
					<div class="nt-toflex">
						<i class="material-icons nt-btncollapse">menu</i>
						<h4>{l s='Filter by tags' mod='ohmproducttags'}</h4>
						<div class="ohmtags-wrapper">
							<ul>
								{foreach $tag_menus as $k => $tag}
									<li data-tagid="{$tag['id_tag']|escape:'htmlall':'UTF-8'}">
										<div>
											<span class="ohmtags-remove-filter" data-tagid="{$tag['id_tag']|escape:'htmlall':'UTF-8'}">
												<i class="material-icons">close</i></span>
											<a href="{$link|escape:'htmlall':'UTF-8'}?tag_id={$tag['id_tag']|escape:'htmlall':'UTF-8'}">{$tag['name']|escape:'htmlall':'UTF-8'}</a>
											<span class="taghole"></span>
										</div>
									</li>
								{/foreach}
							</ul>
						</div>
					</div>

					<div class="ohmtags-wrapper nt-toggler">
						{foreach $tag_menus as $k => $tagm}
							<label class="lblcontainer ovrrde-lbl">
                                <input type="checkbox" data-tagid="{$tagm['id_tag']|escape:'htmlall':'UTF-8'}" class="chkohmtag" {if Tools::getValue('tag_id') == $tagm['id_tag'] || in_array($tagm['id_tag'], $filter_args)}checked{/if}>
                                <span class="checkmark"></span>
                                <span>{$tagm['name']|escape:'htmlall':'UTF-8'}</span>
                            </label>
						{/foreach}
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-10 ot-cwidth80 ohmtags-single-wrapper">

			<div class="ohmtags-wrapper">
				<ul>
					<h5>{l s='Current filters' mod='ohmproducttags'}</h5>
					{foreach $tag_menus as $k => $tag}
						<li data-tagid="{$tag['id_tag']|escape:'htmlall':'UTF-8'}">
							<div>
								<span class="ohmtags-remove-filter" data-tagid="{$tag['id_tag']|escape:'htmlall':'UTF-8'}">
									<i class="material-icons">close</i></span>
								<a href="{$link|escape:'htmlall':'UTF-8'}?tag_id={$tag['id_tag']|escape:'htmlall':'UTF-8'}">{$tag['name']|escape:'htmlall':'UTF-8'}</a>
								<span class="taghole"></span>
							</div>
						</li>
					{/foreach}
				</ul>
			</div>

		  	<div class="products front-producttag-wrapper">
		  		{if count($products)}
				    {foreach from=$products item="product"}
				    	{include file="catalog/_partials/miniatures/product.tpl" product=$product}
				    {/foreach}
				{else}
					<div class="list-empty-msg-front">
						<i class="material-icons">warning</i>
						<span>{l s='No products yet' mod='ohmproducttags'}</span>
					</div>
			    {/if}
		  	</div>

		  	<div class="sg_loader-wrapper">
		  		<div>
				   	<div class="sg_circle-loader">
				    	<div class="sg_checkmark draw"></div>
				   	</div>
			   	</div>
			</div>

		</div>
	</section>
{/block}