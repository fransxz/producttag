{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $tags}
	<div id="productTag-wrapper">
		<h4>{l s='Product Tags' mod='ohmproducttags'}</h4>
		<div class="ohmtags-wrapper">
			<ul>
				{foreach $tags as $k => $tag}
					<li id="li{$tag['id_tag']|escape:'htmlall':'UTF-8'}">
						<span class="ohmtagsnum">{$tag['total_products_count']|escape:'htmlall':'UTF-8'}</span>
						<div>
							<a href="{$link|escape:'htmlall':'UTF-8'}?tag_id={$tag['id_tag']|escape:'htmlall':'UTF-8'}" title="">{$tag['name']|escape:'htmlall':'UTF-8'}</a>
							<span class="taghole"></span>
						</div>
					</li>
					<style>
						.tags-wrapper ul > #li{$tag['id_tag']|escape:'htmlall':'UTF-8'} {
							background-color: {$tag['bg_color']|escape:'htmlall':'UTF-8'};
						}

						.tags-wrapper ul > #li{$tag['id_tag']|escape:'htmlall':'UTF-8'}:after {
						    border-left-color: {$tag['bg_color']|escape:'htmlall':'UTF-8'};
						}

						.tags-wrapper ul > #li{$tag['id_tag']|escape:'htmlall':'UTF-8'} a {
							color: {$tag['text_color']|escape:'htmlall':'UTF-8'};
						}
					</style>
				{/foreach}
			</ul>
		</div>
	</div>
{/if}
