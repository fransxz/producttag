{**
 * NOTICE OF LICENSE.
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Ohm Conception
 *  @copyright 2020 Ohm Conception
 *  @license   license,txt
 *}
 
 <script type="text/javascript">
    
    var securekey = "{$securekey|escape:'htmlall':'UTF-8'}";
    var ajax_url_front = "{$ajax_url_front|cleanHtml nofilter}?tag_id=";
    var current_id_tag = "{Tools::getValue('tag_id')}";
    
</script>