/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$(function() 
{
	$('#productTag-wrapper .chkohmtag').change(function() 
	{

		var id = new Array();
		var tagname = new Array();
		var url = ajax_url_front + $(this).data('tagid');
		

		$('.sg_loader-wrapper').fadeIn();
		$('.ohmtags-single-wrapper .ohmtags-wrapper > ul > li').hide();

		if ($("#productTag-wrapper .ohmtags-wrapper input:checkbox:checked").length == 0)
		{
		    $('.ohmtags-single-wrapper .ohmtags-single-wrapper .ohmtags-wrapper').hide();
		} else {
		    $('.ohmtags-single-wrapper .ohmtags-single-wrapper .ohmtags-wrapper').show();
			$('#productTag-wrapper .ohmtags-wrapper input[type=checkbox]:checked').each(function () 
			{
		        id.push($(this).data("tagid"));
		        tagname.push($(this).next().text());

		        $('li[data-tagid="'+$(this).data("tagid")+'"]').show();

		    });
			
			history.pushState(null, "", ajax_url_front + current_id_tag + '&q=' + id);
		}

	    param = {
	    	tagid : id,
            securekey : securekey,
            action : 'FILTER_TAG',
        }

        $.post(url, param, function(data) {
            
            $('.front-producttag-wrapper').empty().html(data);
			$('.sg_loader-wrapper').fadeOut();
        });
    }).change();


	$('.ohmtags-remove-filter').click(function() {
		$('input:checkbox[data-tagid="'+$(this).data("tagid")+'"]').click();
	});


	$('.nt-btncollapse').click(function() {
		console.log('xx');
		$('.nt-toggler').slideToggle();
	});


	if (window.history && window.history.pushState) {

        $(window).bind('popstate', function() {
            window.location.href = window.location.href;
        });
    }


});