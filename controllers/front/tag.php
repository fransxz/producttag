<?php
/**
 * NOTICE OF LICENSE.
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Ohm Conception
 *  @copyright 2020 Ohm Conception
 *  @license   license,txt
 */

require_once dirname(__FILE__).'../../../../../config/config.inc.php';
require_once dirname(__FILE__) . '/../../ohmproducttags.php';

use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;

class OhmProducttagsTagModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        
        $product_list = [];
        $tag_id = Tools::getValue('tag_id');
        $tag_menu = OhmProductTagClass::getTagData();
        $tagdata = OhmProductTagClass::getTagById($tag_id);

        if ($tagdata) {
            $products = explode(',', $tagdata['id_product']);
            if ($products) {
                foreach ($products as $id_product) {
                    $product_list[] = new Product($id_product, false, $this->context->language->id);
                }
            }
        }

        $products = $this->getProducts($product_list);
        $this->context->controller->addCSS('/views/css/front.css');

        $this->context->smarty->assign('tag_menus', $tag_menu);
        $this->context->smarty->assign('tag', $tagdata);
        // $this->context->smarty->assign('products', $product_list);
        $this->context->smarty->assign('products', $products);
        $this->context->smarty->assign('link', Context::getContext()->link->getModuleLink('ohmproducttags', 'tag'));

        $this->setTemplate('module:ohmproducttags/views/templates/hook/product-tag.tpl');
    }

    protected function getProducts($products)
    {
        // $category = new Category((int) Configuration::get('HOME_ALLPRODUCT_CAT'));
        $category = new Category(2);

        $searchProvider = new CategoryProductSearchProvider(
            $this->context->getTranslator(),
            $category
        );

        $context = new ProductSearchContext($this->context);

        $query = new ProductSearchQuery();

        $nProducts = Configuration::get('HOME_ALLPRODUCTS_NBR');
        if ($nProducts < 0) {
            $nProducts = 12;
        }

        $query
            ->setResultsPerPage(count($products))
            ->setPage(1)
        ;

        if (Configuration::get('HOME_ALLPRODUCT_RANDOMIZE')) {
            $query->setSortOrder(SortOrder::random());
        } else {
            $query->setSortOrder(new SortOrder('product', 'position', 'asc'));
        }

        $result = $searchProvider->runQuery(
            $context,
            $query
        );


        $array = json_decode(json_encode($products), true);

        if (count($array)) {
            foreach ($array as $key => $value) {
                $array[$key]['id_product'] = $value['id'];
                $array[$key]['page'] = array();
            }
        }

        $assembler = new ProductAssembler($this->context);

        $presenterFactory = new ProductPresenterFactory($this->context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new ProductListingPresenter(
            new ImageRetriever(
                $this->context->link
            ),
            $this->context->link,
            new PriceFormatter(),
            new ProductColorsRetriever(),
            $this->context->getTranslator()
        );

        $products_for_template = [];

        foreach ($array as $rawProduct) {
            $products_for_template[] = $presenter->present(
                $presentationSettings,
                $assembler->assembleProduct($rawProduct),
                $this->context->language
            );
        }

        return $products_for_template;
    }

    public function postProcess()
    {
        if (Tools::getValue('action') !== null) {
            $products = [];
            switch (Tools::getValue('action')) {
                case 'FILTER_TAG':
                    $product_list = [];
                    $tagids = Tools::getValue('tagid');
                    
                    if (Tools::getIsset('tagid') && count($tagids)) {
                        foreach ($tagids as $tagid) {
                            $tagdata = OhmProductTagClass::getTagById($tagid);
                            $products[] = explode(',', $tagdata['id_product']);
                        }
                        if ($products) {
                            foreach ($products as $id_product) {
                                foreach ($id_product as $prods) {
                                    $product_list[] = new Product($prods, false, $this->context->language->id);
                                }
                            }
                        }
                        $products = $this->getProducts($product_list);
                        $this->context->smarty->assign('products', $products);
                    } else {
                        $this->context->smarty->assign('no_products', true);
                    }

                    echo $this->context->smarty->fetch(_PS_MODULE_DIR_.'ohmproducttags/views/templates/hook/ajax.tpl');
                    die();
                    break;

                default:
                    // code...
                    break;
            }
        }
    }
}
